## Describe the bug

(Describe the problem clearly and concisely.)


## Expected behavior

(Describe the expected behavior clearly and concisely.)


## Actual behavior

(Describe the actual behavior clearly and concisely.)


## Logs and/or screenshots

(Join any relevant logs and/or screenshot. Please use code blocks (`` ``` ``) to format console output, logs, and code.)


## Context & Configuration

Link to a project, pipeline or job facing the bug: (please provide one if possible)

The issue was reproduced using:

* Version of the template: (type in the version)
* GitLab server(s): (Was it gitlab.com? A self-managed server? Which version? CE / EE? Which license?)
* GitLab runner(s): (type in any relevant information about the GitLab runner(s) you used)


Here is the `.gitlab-ci.yml` file:

```yaml
# Add your .gitlab-ci.yml here, if applicable and useful.

```

(If useful, list configured GitLab CI project and/or group variables.)

Configured GitLab CI project or group variables:

* `VARIABLE_1`
* `VARIABLE_2`
* ...


(Finally add any possible additional useful context info here.)




/label ~"kind/bug" ~"status/needs-investigation"
